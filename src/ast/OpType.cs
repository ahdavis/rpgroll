/*
 * OpType.cs
 * Enumerates types of operations for die equations
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//enum definition
	public enum OpType {
		Addition,
		Subtraction,
		DieRoll
	}
}

//end of enum
