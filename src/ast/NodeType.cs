/*
 * NodeType.cs
 * Enumerates types of AST nodes
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//enum definition
	public enum NodeType {
		Operation, //operation node
		Number //number node
	}
}

//end of enum
