/*
 * NumberNode.cs
 * Defines a class that represents a numeric AST node
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//class declaration
	public class NumberNode : ASTNode {
		//property declaration
		
		//the value of the node
		public int Value {
			get;
			private set;
		}

		/// <summary>
		/// Constructs a NumberNode instance
		/// </summary>
		/// <param name="newValue">
		/// The value of the node
		/// </param>
		public NumberNode(int newValue)
			: base(NodeType.Number, null, null)
		{
			//init the field
			Value = newValue;
		}
		
		/// <summary>
		/// Evaluates the NumberNode
		/// </summary>
		/// <returns>
		/// The value of the node
		/// </returns>
		public override int Evaluate() {
			return Value; //return the value of the node
		}

	}
}

//end of class
