/*
 * OperationNode.cs
 * Defines a class that represents an operator AST node
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//using statement
using System;

//namespace declaration
namespace RPGRoll {
	//class definition
	public class OperationNode : ASTNode {
		//property declaration
		
		//the type of operation this node represents
		public OpType Operation {
			get;
			private set;
		}

		/// <summary>
		/// Constructs an OperationNode instance
		/// </summary>
		/// <param name="newType">
		/// The type of operation the node represents
		/// </param>
		/// <param name="leftBranch">
		/// The left branch of the AST
		/// </param>
		/// <param name="rightBranch">
		/// The right branch of the AST
		/// </param>
		public OperationNode(OpType newType,
					ASTNode leftBranch,
					ASTNode rightBranch)
			: base(NodeType.Operation, leftBranch,
					rightBranch)
		{
			//init the field
			Operation = newType;
		}

		/// <summary>
		/// Evaluates the operation and returns the result
		/// </summary>
		/// <returns>
		/// The result of the operation
		/// </returns>
		public override int Evaluate() {
			//switch on the operation type
			switch(Operation) {
				case OpType.Addition:
					{
						//evaluate the addition
						return 
						LeftBranch.Evaluate() +
						RightBranch.Evaluate();
					}
				case OpType.Subtraction:
					{
						//evaluate the subtraction
						return
						LeftBranch.Evaluate() -
						RightBranch.Evaluate();
					}
				case OpType.DieRoll:
					{
						//create an RNG
						var rng = new Random();

						//get the number of
						//times to roll the die
						int count = 
						LeftBranch.Evaluate();

						//get the number of 
						//sides on the die
						int sides =
						RightBranch.Evaluate();
						
						//declare the return
						//value
						int ret = 0;

						//loop and roll the die
						for(int i = 0; i < count;
							i++) {
							ret +=
							(rng.Next(sides) 
								+ 1);
						}

						//and return the result
						return ret;
					}
			}

			//appease the compiler
			return 0;
		}
		
	}
}

//end of class
