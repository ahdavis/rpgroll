/*
 * ASTNode.cs
 * Defines a class that represents an AST node
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//class definition
	public abstract class ASTNode {
		//property declarations
		
		//the type of the ASTNode
		public NodeType Type {
			get;
			private set;
		}

		//the left branch of the AST
		public ASTNode LeftBranch {
			get;
			private set;
		}

		//the right branch of the AST
		public ASTNode RightBranch {
			get;
			private set;
		}

		/// <summary>
		/// Constructs an ASTNode instance
		/// </summary>
		/// <param name="newType">
		/// The type of the AST node
		/// </param>
		/// <param name="newLeftBranch">
		/// The new left branch of the AST
		/// </param>
		/// <param name="newRightBranch">
		/// The new right branch of the AST
		/// </param>
		protected ASTNode(NodeType newType, 
				ASTNode newLeftBranch,
				ASTNode newRightBranch) {
			//init the fields
			Type = newType;
			LeftBranch = newLeftBranch;
			RightBranch = newRightBranch;
		}

		/// <summary>
		/// Evaluates an AST and returns the result
		/// </summary>
		/// <returns>
		/// The result of traversing and evaluating the AST
		/// </returns>
		public abstract int Evaluate();
	}
}

//end of class
