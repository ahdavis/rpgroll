/*
 * TokenType.cs
 * Enumerates types of parser tokens
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//enum definition
	public enum TokenType {
		Number, //numeric token
		LParen, //left parenthesis token
		RParen, //right parenthesis token
		DieOp, //die operator token
		Plus, //plus token
		Minus, //minus token
		EndOfLine //EOL token
	}
}

//end of enum
