/*
 * Lexer.cs
 * Defines a class that represents a lexical analyzer
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//using statements
using System;
using System.Text;

//namespace declaration
namespace RPGRoll {
	//class definition
	public class Lexer {
		//fields
		private readonly string _text; //the text being lexed
		private int _pos; //the current position in the text
		private char _curChar; //the current character being lexed

		//no properties
		
		/// <summary>
		/// Constructs a Lexer instance
		/// </summary>
		/// <param name="text">
		/// The text to be analyzed
		/// </param>
		public Lexer(string text) {
			//init the fields
			_text = text;
			_pos = 0;
			_curChar = _text[_pos];
		}

		/// <summary>
		/// Returns the next token consumed from the input
		/// </summary>
		/// <returns>
		/// The next token consumed from the input
		/// </returns>
		public Token NextToken() {
			//loop through the text
			while(_curChar != '\0') {
				//handle whitespace
				if(_curChar == ' ') {
					SkipWhitespace();
					continue;
				}

				//handle integers
				if(char.IsDigit(_curChar)) {
					return new Token(TokenType.Number,
								Int());
				}

				//handle left parentheses
				if(_curChar == '(') {
					Advance();
					return new Token(TokenType.LParen,
								'(');
				}

				//handle right parentheses
				if(_curChar == ')') {
					Advance();
					return new Token(TokenType.RParen,
								')');
				}

				//handle plus signs
				if(_curChar == '+') {
					Advance();
					return new Token(TokenType.Plus,
								'+');
				}

				//handle minus signs
				if(_curChar == '-') {
					Advance();
					return new Token(TokenType.Minus,
								'-');
				}

				//handle die operators
				if((_curChar == 'd') || 
					(_curChar == 'D')) {
					Advance();
					return new Token(TokenType.DieOp,
								'd');
				}

				//no match, so exit the program
				Console.WriteLine("Unknown character {0}",
							_curChar);
				Environment.Exit(1);
			}

			//and return an EOL token
			return new Token(TokenType.EndOfLine, '\0');
		}


		/// <summary>
		/// Advances the lexing position in the text
		/// </summary>
		private void Advance() {
			_pos++;
			if(_pos > _text.Length - 1) {
				_curChar = '\0';
			} else {
				_curChar = _text[_pos];
			}

			
		}

		/// <summary>
		/// Skips whitespace in the text
		/// </summary>
		private void SkipWhitespace() {
			while((_curChar == ' ') && (_curChar != '\0')) {
				Advance();
			}
		}

		/// <summary>
		/// Returns an integer consumed from the text
		/// </summary>
		/// <returns>
		/// An integer consumed from the text
		/// </returns>
		private int Int() {
			//create a stringbuilder
			StringBuilder sb = new StringBuilder();

			//loop and generate a string
			while(char.IsDigit(_curChar) &&
				(_curChar != '\0')) {
				sb.Append(_curChar);
				Advance();
			}

			//and return the string converted to an integer
			return int.Parse(sb.ToString());
		}
	}
}

//end of class
