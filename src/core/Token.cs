/*
 * Token.cs
 * Defines a class that represents a parser token
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//no using statements

//namespace declaration
namespace RPGRoll {
	//class definition
	public class Token {
		//property declarations
		
		//the type of the Token
		public TokenType Type {
			get;
			private set;
		}

		//the integer value of the Token
		public int IntValue {
			get;
			private set;
		}

		//the character value of the Token
		public char CharValue {
			get {
				return (char)IntValue;
			}

			private set {
				IntValue = (int)value;
			}
		}

		/// <summary>
		/// Constructs a Token instance from an integer
		/// </summary>
		/// <param name="newType">
		/// The type of the Token
		/// </param>
		/// <param name="newValue">
		/// The value of the Token
		/// </param>
		public Token(TokenType newType, int newValue) {
			//init the fields
			Type = newType;
			IntValue = newValue;
		}

		/// <summary>
		/// Constructs a Token instance from a character
		/// </summary>
		/// <param name="newType">
		/// The type of the Token
		/// </param>
		/// <param name="newValue">
		/// The value of the Token
		/// </param>
		public Token(TokenType newType, char newValue) {
			//init the fields
			Type = newType;
			CharValue = newValue;
		}
	}
}

//end of class
