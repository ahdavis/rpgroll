/*
 * main.cs
 * Main code file for rpgroll
 * Created by Andrew Davis
 * Created on 9/21/2018
 * Copyright 2018 Andrew Davis
 * Licensed under the MIT License
 */

//using statements
using System;

//namespace declaration
namespace RPGRoll {
	//class definition
	public class RPGRoll {
		/// <summary>
		/// Main entry point for the program
		/// </summary>
		/// <param name="args">
		/// The command-line arguments to the program
		/// </param>
		static void Main(string[] args) {
			//display a temporary message
			Console.WriteLine(
				"RPGRoll is under construction.");
			Console.WriteLine(
				"Check back later for more features.");
		}
	}
}

//end of program
